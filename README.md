## Maven Multi-Module Project

A multi-module project is built from an aggregator POM that manages a group of submodules. In most cases, the aggregator is located in the project's root directory and must have packaging of type pom. The submodules are regular Maven projects, and they can be built separately or through the aggregator POM.

## Running the Project

```
cd working_directory
git clone https://gitlab.com/pgichure/maven-multi-module-project.git
cd maven-multi-module-project
mvn spring-boot-run
```